# frozen_string_literal: true

FactoryBot.define do
  factory :distribution do
    dataset
    node
  end
end
