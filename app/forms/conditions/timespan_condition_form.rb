# frozen_string_literal: true

class TimespanConditionForm < ApplicationForm
  field :duration
end
