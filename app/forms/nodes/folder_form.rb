# frozen_string_literal: true

class FolderForm < NodeForm
  field :display_name, datatype: NS.xsd.string
end
