# frozen_string_literal: true

class DataspacePolicy < ApplicationPolicy
  def show?
    true
  end
end
