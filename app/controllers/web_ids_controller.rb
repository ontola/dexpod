# frozen_string_literal: true

class WebIdsController < ApplicationController
  active_response :show
end
