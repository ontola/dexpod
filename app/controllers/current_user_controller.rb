# frozen_string_literal: true

class CurrentUserController < ApplicationController
  active_response :show
end
