# frozen_string_literal: true

class ProfilesController < ApplicationController
  active_response :show
end
